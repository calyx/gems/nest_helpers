module NestHelpers
  module FormHelper

    ##
    ## BUTTONS
    ##

    private

    #
    # Like button_to, but with fewer options and assumes bootstrap
    #
    # options:
    #  :class -- css for the button
    #  :command -- optional command_x parameter
    #
    def button_form(url:, method:, options:, &block)
      button_class = options[:class]
      if button_class !~ /^btn-/
        button_class = 'btn btn-outline-' + button_class
      end
      method_element = hidden_field_tag("_method", method)
      token_element  = token_tag(nil, form_options: {action: url, method: method})
      button_element = content_tag('button', {type: 'submit', class: button_class}, &block)
      inner_tags = method_element.safe_concat(token_element).safe_concat(button_element)
      if options[:command]
        inner_tags = inner_tags.safe_concat(
          hidden_field_tag("command_#{options[:command]}", "true")
        )
      end
      content_tag("form", inner_tags, {action: url, method: 'post'})
    end

    protected

    def button_put(name, url, options={})
      button_form(url: url, method: 'put', options: options) { name }
    end

    def button_post(name, url, options={})
      button_form(url: url, method: 'post', options: options) { name }
    end

  end
end
