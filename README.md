This gem provide several general-purpose helpers for Ruby on Rails
applications.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'nest_helpers'
```

Then run:

```bash
$ bundle
```

## License

The gem is available as open source under the terms of the MIT License.
